#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 27 17:48:35 2023

@author: jago
"""

import numpy as np
import quimb as q


h = q.up()
v = q.down()
d = q.plus()
ad = q.minus()

def generate_random_bits(length=10):
	return np.random.choice([0,1], length)

def generate_random_bases(length=10):
	# 0 corresponds with r basis
	# 1 corresponds with d basis
	return np.random.choice([0,1], length)

def generate_photons(rbits, rbases):

	photon_map = {0.0: h, 1.0: v, 0.1:d, 1.1: ad}
	photons = [photon_map[source[0] + source[1]/10] for source in zip(rbits, rbases)]
	return photons

def measurement(basis=0, state=h, state_out=False):
	photon_map = {0.0: h, 1.0: v, 0.1:d, 1.1: ad}
	probability = 1
	if basis==0:
		# Add a trivial C-number to avoid type mismatch
		projector = np.outer(h,h) + 0.0j
		probability = state.conj().T @ projector @ state
	elif basis==1:
		projector = np.outer(d,d) + 0.0j
		probability = state.conj().T @ projector @ state

	if np.random.uniform(0, 1) < probability.real:
		result = 0
	else:
		result = 1
	# results: 0.0 = h, 1.0 = v, 0.1 = d, 1.1 = ad
	# Not sure how stable this implementation is but I think for just 0 and 1
	# it's fine
	if state_out:
		# If requested return actual state instead of just symbolic outcome
		# required for eavesdropping. It's just a technicality as they're both
		# equivalent
		return photon_map[result + basis/10]
	else:
		return result + basis/10

def full_measurement(bases, states):
	return np.array([measurement(*i) for i in zip(bases, states)])

def extract_bits(measurements):
	# extract bits from the measured state, similar to how Alice generated her
	# states
	return measurements.astype(int)

def compare_bits(bits1, bits2):
	return np.where(bits1 == bits2)[0]

def pick_subset(array, sublength):
	indices = np.arange(len(array))
	return np.random.choice(indices, sublength, replace=False)


def extract_key(bits, base_intersect, base_subset):
	# Returns the complement of matching bits to the eavesdropping test subset
# 	breakpoint()
	return np.delete(bits[base_intersect], base_subset)

def xor_encrypt(message, key):
	# If message is shorter than key, padd it with zeros (<= keep this info secret)
	# If key is shorter generate a new or additional key
	diff = len(key) - len(message)

	if diff >= 0:
		message = np.concatenate((np.zeros(diff, dtype=int), message))
# 		# Alice should probably let Bob know the message was padded
		return message ^ key
	else:
		print("Generate larger key")

def message2binary(message):
	message = bin(message)
	return np.array(list(message[2:]), dtype=int)

def eavesdrop(photons, bases=[0,], indices=[0,]):
	# I know about the no-copy theorem but it's not applicable to Python
	ephotons = photons.copy()
	for i in zip(bases, indices):

		ephotons[i[1]] = measurement(i[0], photons[i[1]], state_out=True)
	return ephotons


# Alice (private)
amessage = message2binary(42)
abits = generate_random_bits(20)
abases = generate_random_bases(20)
aphotons = generate_photons(abits, abases)


# Eavesdropper (optional)
aphotons = eavesdrop(aphotons,[0,1], [0,1])


# Bob (private)
bbases = generate_random_bases(20)
bmeasurements = full_measurement(bbases, aphotons)
bbits = extract_bits(bmeasurements)

# Alice + Bob (public)
base_intersect = compare_bits(abases, bbases)
base_subset = pick_subset(base_intersect, 3)
# If no eavesdropping then #bit_subset_intersect == #base_intersect
# Otherwise #bit_subset_intersect can be less
bit_subset_intersect = compare_bits(abits[base_intersect][base_subset],
									 bbits[base_intersect][base_subset])

# >>>>>>>  Abort here if sus <<<<<<<
if len(bit_subset_intersect) < len(base_subset): print('Compromised')


# Alice + Bob (private)
qkey = extract_key(bbits, base_intersect, base_subset)
# encrypt
smessage = xor_encrypt(amessage, qkey)
# decrypt
# If NoneType error it means the key was too short
bmessage = xor_encrypt(smessage, qkey)